/**
 * @description 删除购物车
 * @author unhejing (成都-敬宇杰)
 * @date 2020-02-22 下午1:39:01
 */
'use strict';
const db = uniCloud.database()
exports.main = async (event, context) => {
	//event为客户端上传的参数
	event.user_id = "1"
	if (event.user_id == '' || !event.user_id) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少用户id'
		}
	}

	//event.ids = '5e50c27780909a004e07f4ce'
	if (event.ids == '' || !event.ids) {
		return {
			success: false,
			code: -1,
			msg: '参数错误，缺少购物车id'
		}
	}
	let idArr = event.ids.split(",");
	console.log("需要删除的购物车信息:" + JSON.stringify(idArr))
	const dbCmd = db.command;
	let deleteRes = await db.collection('cart').where({
		_id: dbCmd.in(idArr)
	}).remove();
	console.log("删除结果：" + JSON.stringify(deleteRes))
	if (deleteRes.deleted == 0) {
		return {
			success: false,
			code: -1,
			msg: '服务器内部错误，删除失败'
		}
	}
	
	console.log("************************华丽的分割线（以下为获取购物车列表）************************")
	// 获取所有购物车数据
	let allRes = await db.collection('cart').where({
		'user_id': event.user_id
	}).orderBy("create_time", "desc").get()
	if (allRes.data.length == 0) {
		return {
			success: false,
			code: -1,
			msg: '服务器内部错误'
		}
	}

	let cartList = allRes.data;
	let productIds = new Array();
	cartList.map(o => productIds.push(o.product_id))
	console.log("获取商品相关信息：" + JSON.stringify(productIds))
	// 获取商品相关信息
	let productRes = await db.collection('product').where({
		_id: dbCmd.in(productIds)
	}).get()
	let productList = productRes.data;
	console.log("productList:" + JSON.stringify(productList))
	let productMap = new Map();
	productList.map(o => {
		productMap.set(o._id, o);
	})
	console.log("productMap:" + JSON.stringify(productMap.size))
	// 设置购物车信息
	cartList = await cartList.map(o => {
		let product = productMap.get(o.product_id);
		if (product) {
			o.product_code = product.product_code
			o.product_name = product.product_name
			o.sale_price = product.sale_price
			o.book_price = product.book_price
			o.primary_img = product.primary_img
			o.publish_status = product.publish_status
		}
		return o;
	})
	// 设置商品信息
	if (allRes.id || allRes.affectedDocs >= 1) {
		return {
			success: true,
			code: 0,
			msg: '成功',
			data: cartList
		}
	}
	return {
		success: false,
		code: -1,
		msg: '服务器内部错误'
	}
};
