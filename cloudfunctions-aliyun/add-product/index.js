'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function isEmpty(v) {
	switch (typeof v) {
		case 'undefined':
			return true;
		case 'string':
			if (v.replace(/(^[ \t\n\r]*)|([ \t\n\r]*$)/g, '').length == 0) return true;
			break;
		case 'boolean':
			if (!v) return true;
			break;
		case 'number':
			if (isNaN(v)) return true;
			break;
		case 'object':
			if (null === v || v.length === 0) return true;
			for (var i in v) {
				return false;
			}
			return true;
	}
	return false;
}
var stringUtil = {
	isEmpty
};

const {isEmpty: isEmpty$1} = stringUtil;
const db = uniCloud.database();
var main = async (event, context) => {
	/**
	 * 模拟数据
	 */
	// event.product_name = "【自营】知牧锡盟有机羔羊肉片 325g 盒装"
	// event.sale_unit = "盒"
	// event.book_price = 65.55
	// event.sale_price = 65.55
	// event.primary_img =
	// 	"http://pic3.womai.com/upload/601/603/606/64306/280375/82402/82506/547183/547183_1_pic1242_6580.jpg"
	// event.introduction =
	// 	"PHAgc3R5bGU9InRleHQtYWxpZ246IGNlbnRlciI+PHNwYW4gc3R5bGU9ImZvbnQtc2l6ZTogMTRweCI+PHNwYW4+PHNwYW4+PHNwYW4gc3R5bGU9ImNvbG9yOiAjZmYwMDAwIj48c3Bhbj48c3Ryb25nPjxzcGFuIHN0eWxlPSJsaW5lLWhlaWdodDogMjAwJSI+5Li65oKo6LSt54mp5pa55L6/77yM6LSt5Lmw6K+l5ZWG5ZOB5YmN6aG75LuU57uG6ZiF6K+7PGEgdGFyZ2V0PSJfYmxhbmsiIGhyZWY9Imh0dHA6Ly93d3cud29tYWkuY29tL0luZm8vQWJvdXRVc0RldGFpbC1pZD0xMDI3MDEuaHRtIj48dT48c3BhbiBzdHlsZT0iY29sb3I6ICNmZjAwMDAiPiZsZHF1bzs8L3NwYW4+PC91PjwvYT48c3BhbiBzdHlsZT0iY29sb3I6ICNmZjAwMDAiPjxzcGFuIHN0eWxlPSJmb250LXNpemU6IDE2cHgiPjxzdHJvbmc+PGEgdGFyZ2V0PSJfYmxhbmsiIGhyZWY9Imh0dHA6Ly93d3cud29tYWkuY29tL0luZm8vQWJvdXRVc0RldGFpbC1pZD0xMDI3MDEuaHRtIj48dT48c3BhbiBzdHlsZT0iY29sb3I6ICNmZjAwMDAiPjxzcGFuIHN0eWxlPSJmb250LXNpemU6IDE0cHgiPjxzcGFuPjxzcGFuIHN0eWxlPSJsaW5lLWhlaWdodDogMjAwJSI+PHNwYW4+PHNwYW4+5oiR5Lmw572R5p6c6JSs55Sf6bKc6LSt5Lmw6K+05piO77yI54K55Ye75p+l55yL77yJPC9zcGFuPjwvc3Bhbj48L3NwYW4+PC9zcGFuPjwvc3Bhbj48L3NwYW4+PC91PjwvYT48L3N0cm9uZz48L3NwYW4+PC9zcGFuPjwvc3Bhbj48L3N0cm9uZz48L3NwYW4+PC9zcGFuPjwvc3Bhbj48L3NwYW4+PC9zcGFuPjwvcD4NCjxwIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXIiPjxzdHJvbmc+PHNwYW4gc3R5bGU9ImJhY2tncm91bmQtY29sb3I6IHJnYigyNTUsIDI1NSwgMCk7IGZvbnQtZmFtaWx5OiAiIGFyaWFsPSIiIHVuaWNvZGU9IiI+5paw6ICB5YyF6KOF5pu05pu/5Lit77yM6ZqP5py65Y+R6LSn5ZOm77yBPC9zcGFuPjwvc3Ryb25nPiZuYnNwOzwvcD4NCjxwIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXIiPjxhIGhyZWY9Imh0dHA6Ly93d3cud29tYWkuY29tL0luZm8vQWJvdXRVc0RldGFpbC1pZD0xMDc4MzQ0NC5odG0iIHRhcmdldD0iX2JsYW5rIj48aW1nIHNyYz0iaHR0cDovL3BpYy53b21haS5jb20vdXBsb2FkLzIwMTgvMTIvMjEvMjAxODEyMjEwNTQ3NDIzLmpwZyIgd2lkdGg9IjczMCIgaGVpZ2h0PSI3MzAiIGFsdD0iIiAvPjwvYT48L3A+DQo8cCBzdHlsZT0idGV4dC1hbGlnbjogY2VudGVyIj48aW1nIHNyYz0iaHR0cDovL3BpYy53b21haS5jb20vdXBsb2FkLzIwMTcvMTAvMTIvMjAxNzEwMTIwNTE3NTQ5NjUuanBnIiB3aWR0aD0iNzAwIiBoZWlnaHQ9IjI3MDAiIGFsdD0iIiAvPjwvcD4NCjxwIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXIiPjxpbWcgc3JjPSJodHRwOi8vcGljLndvbWFpLmNvbS91cGxvYWQvMjAxNy8zLzIwLzIwMTcwMzIwMDk0NDM2OTYyLmpwZyIgd2lkdGg9IjczMCIgaGVpZ2h0PSI3MzAiIGFsdD0iIiAvPjwvcD4NCjxwIHN0eWxlPSJ0ZXh0LWFsaWduOiBjZW50ZXIiPjxpbWcgc3JjPSJodHRwOi8vcGljLndvbWFpLmNvbS91cGxvYWQvMjAxNy8zLzIwLzIwMTcwMzIwMDk0NTA4OTAwLmdpZiIgd2lkdGg9IjczMCIgaGVpZ2h0PSIxMDMyIiBhbHQ9IiIgLz48L3A+DQo8cCBzdHlsZT0idGV4dC1hbGlnbjogY2VudGVyIj48aW1nIHNyYz0iaHR0cDovL3BpYy53b21haS5jb20vdXBsb2FkLzIwMTcvMy8yMC8yMDE3MDMyMDA5NDQ1MjE3NS5naWYiIHdpZHRoPSI3MzAiIGhlaWdodD0iMTAzMiIgYWx0PSIiIC8+PC9wPg=="
	// event.otherImgs = [{
	// 		img_path: "http://pic3.womai.com/upload/601/603/606/64306/280375/82402/82506/547183/547183_1_pic1242_6580.jpg",
	// 		is_primary: 1
	// 	},
	// 	{
	// 		img_path: "http://pic3.womai.com/upload/601/603/606/64306/280375/82402/82506/547183/547183_1_pic1242_6580.jpg",
	// 		is_primary: 0
	// 	},
	// 	{
	// 		img_path: "http://pic3.womai.com/upload/601/603/606/64306/280375/82402/82506/547183/547183_8_pic1242_9044.jpg",
	// 		is_primary: 0
	// 	}
	// ]
	//event为客户端上传的参数
	console.log('event:' + event);
	console.log("校验数据...");
	let checkRes = checkData(event);
	if (!checkRes.success) {
		console.log("校验数据失败！！！！");
		return checkRes;
	}
	console.log("校验数据成功^_^");
	console.log("*****************添加商品信息start*****************");
	// 生成随机数（用于商品编号）
	var randomNum = "";
	for (var i = 0; i < 4; i++) {
		randomNum += Math.floor(Math.random() * 10);
	}
	let currentTime = new Date().getTime();
	let productCode = "SP" + currentTime + randomNum;
	// 构建商品数据
	let product = {
		product_code: productCode, //string 商品编码
		product_name: event.product_name, //string 商品名称
		sale_price: parseFloat(event.sale_price), //double 商品销售价格
		book_price: parseFloat(event.book_price), //double 商品代订价格
		sale_unit: event.sale_unit, //string 销售单位
		introduction: event.introduction, //string html格式展示， 产品描述
		primary_img: event.primary_img, //string 商品主图
		publish_status: 1, //上下架状态,0 下架 1 上架(默认上架)
		create_time: currentTime, // 时间戳 GMT, 创建时间
		update_time: currentTime, // 时间戳 GMT, 更新时间
		create_by: "admin", //string 创建人
		update_by: "admin" //string 更新人
	};
	console.log("构建商品数据:" + JSON.stringify(product));
	// 构建图片数据
	let otherImgs = new Array();
	otherImgs = event.otherImgs.map(o => {
		return {
			img_path: o.img_path,
			is_primary: parseInt(o.is_primary)
		}
	});
	console.log("构建图片数据:" + JSON.stringify(otherImgs));
	// 保存商品数据
	let productRes = await db.collection('product').add(product);
	console.log("商品添加响应结果：" + JSON.stringify(productRes));
	if (productRes.id) {
		otherImgs.map(o => {
			o.product_id = productRes.id;
		});
	} else {
		return {
			success: false,
			code: -1,
			msg: '保存商品数据异常'
		}
	}
	console.log("即将保存图片数据：" + JSON.stringify(otherImgs));
	for (var i = 0; i < otherImgs.length; i++) {
		await db.collection('product_img').add(otherImgs[i]);
	}

	console.log("*****************添加商品信息end*****************");
	return {
		success: true,
		code: 0,
		msg: '添加成功'
	}
};

function checkData(event) {
	if (isEmpty$1(event.product_name)) {
		return {
			success: false,
			code: -1,
			msg: '商品名称必填'
		}
	}

	if (isEmpty$1(event.sale_unit)) {
		return {
			success: false,
			code: -1,
			msg: '销售单位必填'
		}
	}

	if (isEmpty$1(event.book_price)) {
		return {
			success: false,
			code: -1,
			msg: '订货价必填'
		}
	}

	if (isEmpty$1(event.sale_price)) {
		return {
			success: false,
			code: -1,
			msg: '销售价必填'
		}
	}

	if (isEmpty$1(event.primary_img)) {
		return {
			success: false,
			code: -1,
			msg: '请选择商品主图'
		}
	}

	if (!event.introduction || event.introduction == '') {
		return {
			success: false,
			code: -1,
			msg: '商品描述必填'
		}
	}

	if (!Array.isArray(event.otherImgs) || event.otherImgs.length == 0) {
		return {
			success: false,
			code: -1,
			msg: '商品轮播图必传'
		}
	}

	return {
		success: true,
		msg: "校验通过"
	}
}

var addProduct = {
	main: main
};

exports.default = addProduct;
exports.main = main;
