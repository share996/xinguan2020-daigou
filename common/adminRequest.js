/**
 * 管理端请求接口
 */
import network from "./network.js";
  
let addProduct = function(options){  // 管理员添加商品
	return network({
		url:"add-product",
		data:options ? options.data : {}
	})
}

let updateProductStatus = function(options){
	return network({
		url:"update-product-status",
		data:options ? options.data : {},
		isShowLoading:options ? (options.isShowLoading || false) : false
	})
}

export {
	addProduct,
	updateProductStatus
}