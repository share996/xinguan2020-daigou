/**
 * @param {Object} options
 * 各个 数据请求 方法
 */
import network from "./network.js";
const getProductList = function (options) {  // 首页商品列表
  return network({
     url:"get-product-list",
	 data:options ? options.data : {},
	 isShowLoading:options ? (options.isShowLoading || false) : false  //是否为首页
  })
};

const getProductDetail = function(options){  //商品详情
	return network({
	   url:"get-product-detail",
	   data:options ? options.data : {}
	})
}

const addCart = function(options){  //商品详情
	return network({
	   url:"add-cart",
	   data:options ? options.data : {}
	})
}

const getCartList = function(options){  //获取购物车数据
	return network({
	   url:"get-cart-list",
	   data:options ? options.data : {}
	})
}

const deleteCart = function(options){ //删除购物车商品
	return network({
	   url:"delete-cart",
	   data:options ? options.data : {}
	})
}

const updateCartNum = function(options){ //更新购物车数量
	return network({
	   url:"update-cart-num",
	   data:options ? options.data : {}
	})
}

const createOrder = function(options){ //创建订单
	return network({
	   url:"create-order",
	   data:options ? options.data : {}
	})
}

const updateOrderStatus = function(options){ //更新订单状态
	return network({
	   url:"update-order-status",
	   data:options ? options.data : {}
	})
}

const getOrderList = function(options){ //获取订单列表
	return network({
	   url:"get-order-list",
	   data:options ? options.data : {}
	})
}

const getUserInfo = function (options) {  // 获取用户信息
  return network({
     url:"get-user-info",
	 data:options ? options.data : {}
  })
};

const updateUser = function(options){   //修改用户信息
	return network({
	   url:"update-user",
	   data:options ? options.data : {}
	})
}

export {
	getUserInfo,
	updateUser,
	getProductList,
	getCartList,
	deleteCart,
	updateCartNum,
	getProductDetail,
	addCart,
	createOrder,
	updateOrderStatus,
	getOrderList
}

