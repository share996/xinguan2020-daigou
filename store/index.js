import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin: false, //登录状态
		userInfo: {}, // 缓存用户信息
		token:"",     // token
		openid:"",//微信登录 唯一标识
		adminToken:"",
		adminLogin:false
		
	},
	mutations: {
		// 管理员登录退出
		adminLogin(state, data){
			state.adminToken = data.adminToken;
			state.adminLogin = true;
			uni.setStorageSync("adminToken",data.adminToken) 
		},
		
		// 普通用户登录退出
		login(state, data) {
			state.hasLogin = true;
			state.userInfo = data.userInfo;
			state.token = data.token;
			uni.setStorageSync("token",data.token) 
			uni.setStorageSync("userInfo",data.userInfo) //
			uni.setStorageSync("openid",data.openid)
		},
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			state.token = "";
			state.openid = "";
			try{
				uni.removeStorageSync('token');
				uni.removeStorageSync('userInfo');
				uni.removeStorageSync('openid');
			}catch(e){}
		}
	},
	actions: {
	
	}
})

export default store
